<?php
/**
 * @file
 * This module allows to pass form element default values into URLs and substitute them into form elements.
 */
function elementdefaults_menu() {
  $items['admin/settings/elementdefaults'] = array(
    'title' => 'Form element defaults',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('elementdefaults_form'),
    'access arguments' => array('elementdefaults settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
function elementdefaults_perm() {
  return array('elementdefaults settings');
}
function elementdefaults_form(&$form_state) {
  $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $form['page_vis_settings']['elementdefaults_visibility_mode'] = array(
    '#type' => 'radios',
    '#title' => t("Form elements initialization on specific pages"),
    '#options' => array(t('Enable on every page except the listed pages.'),
                        t('Enable on only the listed pages.'),
                        t('Enable if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).')),
    '#default_value' => variable_get('elementdefaults_visibility_mode', 0),
  );
  $form['page_vis_settings']['elementdefaults_visibility_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('elementdefaults_visibility_pages', ''),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')) .' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>')),
  );
  return system_settings_form($form);
}
function elementdefaults_form_alter(&$form, $form_state, $form_id) {
  if (elementdefaults_enable_path()) {
    $request=$_GET;
    unset($request['q']);
    unset($request['destination']);
    if (count($request)) {
      elementdefaults_set_value($form, $request);
    }
  }
}

function elementdefaults_set_value(&$element, $request) {
  $children = array();
  $children = element_children($element);
  if (isset($children[0]) && $children[0]===0) {
    return;
  }
  foreach ($children as $pos => $form_element) {
    if (isset($element[$children[$pos]]['#type'])) $type = $element[$children[$pos]]['#type'];
    else $type = '';
    if (($type !== "value") && ($type !== "token") && ($type !== "submit") && ($type !== "fieldset") && ($type !== "weight")) {
      if (isset($request[$form_element])) {
        $value = explode('^', $request[$form_element]);
        $vals = explode("||", $value[0]);
        $mode = $value[1];
        switch ($type) {
          case "date": {
            if (isset($element[$children[$pos]]['#webform_component'])) {
              $element[$children[$pos]]['#webform_component']['value']= $vals[0];
            }
            break;
          }
          case "checkboxes":
          case "select": {
            if ($element[$children[$pos]]['#multiple'] || ($type==="checkboxes")) {
              $element[$children[$pos]]['#default_value'] = array();
              foreach ($vals as $vkey => $val) {
                $element[$children[$pos]]['#default_value'][$vkey] = $vals[$vkey];
              }
            }
            else {
              $element[$children[$pos]]['#default_value'] = $vals[0];
            }
            break;
          }
          case "nodereference_select":
          case "userreference_select":
          case "nodereference_buttons":
          case "userreference_buttons":
          case "optionwidgets_select":
          case "optionwidgets_buttons": {
            if (($type=="userreference_buttons") || ($type=="userreference_select")) $tp='uid';
            elseif (($type=="nodereference_buttons") || ($type=="nodereference_select")) $tp='nid';
            else $tp='value';
            foreach ($vals as $vkey => $val) {
              if (isset($element[$children[$pos]]['#default_value'][$vkey])) {
                $element[$children[$pos]]['#default_value'][$vkey][$tp] = $val;
              }
            }
            break;
          }
          case "optionwidgets_onoff": {
            $element[$children[$pos]]['#default_value'][0]['value'] = $vals[0];
            break;
          }
          default: {
            if (isset($element[$children[$pos]][0])) {
              foreach ($vals as $vkey => $val) {
                if (isset($element[$children[$pos]][$vkey])) {
                  if (isset($element[$children[$pos]][$vkey]['#type'])) {
                    $type=$element[$children[$pos]][$vkey]['#type'];
                    if ($type=='userreference_autocomplete') $tp='uid';
                    elseif ($type=='nodereference_autocomplete') $tp='nid';
                    else $tp='';
                    if (count($element[$children[$pos]]['#default_value']>1)) {
                      foreach ($vals as $vkey => $val) {
                        if ($tp!=='') $element[$children[$pos]][$vkey]['#default_value'][$tp] = $val;
                        else $element[$children[$pos]][$vkey]['#default_value']['value'] = $val;
                      }
                    }
                    else {
                      $element[$children[$pos]][0]['#default_value'][$tp] = $vals[0];
                    }
                  }
                  else {
                    $element[$children[$pos]][$vkey]['#default_value']['value'] = $val;
                  }
                }
              }
            }
            else {
              $element[$children[$pos]]['#default_value'] = $vals[0];
            }
          }
        }
        if (isset($mode) && ($mode!=='')) {
          elementdefaults_set_mode($element, $children, $pos, $mode, $type);
        }
      }
    }
    elementdefaults_set_value($element[$children[$pos]], $request);
  }
}
function elementdefaults_set_mode(&$element, $children, $pos, $mode, $type) {
  switch ($mode) {
    case '0': {//hide
      $element[$children[$pos]]['#prefix']="<div class='elementdefaults-hidden' style='display: none'>". $element[$children[$pos]]['#prefix'];
      $element[$children[$pos]]['#suffix']=$element[$children[$pos]]['#suffix'] ."</div>";
      break;
    }
    case '1': {//conver to markup
      $markup=elementdefaults_to_markup($element, $children, $pos, $type);
      $element[$children[$pos]]['#prefix']="<div class='elementdefaults-hidden' style='display: none'>" . $element[$children[$pos]]['#prefix'];
      $element[$children[$pos]]['#suffix']=$element[$children[$pos]]['#suffix'] ."</div>";
      $element[$children[$pos]]['#suffix'] .="<div class='elementdefaults-markup'>". $markup['label'] . $markup['value'] ."</div>";
      break;
    }
    case '2': {//disable
      switch ($type) {
        case "date":
        case "checkboxes":
        case 'number':
        case 'date_combo':
        case 'text_textarea':
        case 'text_textfield':
        case "nodereference_select":
        case "userreference_select":
        case "nodereference_buttons":
        case "userreference_buttons":
        case "optionwidgets_select":
        case "optionwidgets_buttons":
        case "optionwidgets_onoff":
        case "userreference_autocomplete":
        case "nodereference_autocomplete": {
          break;
        }
        default: {
          $disabled=elementdefaults_to_disabled($element, $children, $pos, $type);
          $element[$children[$pos]]['#prefix']="<div class='elementdefaults-hidden' style='display: none'>". $element[$children[$pos]]['#prefix'];
          $element[$children[$pos]]['#suffix']=$element[$children[$pos]]['#suffix'] ."</div>";
          $element[$children[$pos]]['#suffix'] .=$disabled;
        }
      }
      break;
    }
  }

}

function elementdefaults_to_markup(&$element, $children, $pos, $type) {
  $markup=array();
  $label='';
  $value='';
  if (isset($element[$children[$pos]]['#title'])) {
    $label="<span class='elementdefaults-label'>". $element[$children[$pos]]['#title'] .":&nbsp;</span>";
  }
  switch ($type) {
    case 'date': {
      $value = $element[$children[$pos]]['#webform_component']['value'];
      break;
    }
    case 'number':
    case 'date_combo':
    case 'text_textarea':
    case 'text_textfield':
    case 'userreference_autocomplete':
    case 'nodereference_autocomplete': {
      if ($type==='userreference_autocomplete') $tp='uid';
      elseif ($type==='nodereference_autocomplete') $tp='nid';
      else $tp='value';
      $child=element_children($element[$children[$pos]]);
      $val=array();
      foreach ($child as $elem) {
        if ($element[$children[$pos]][$elem]['#default_value'][$tp]!='') $val[]=$element[$children[$pos]][$elem]['#default_value'][$tp];
      }
      $value=implode(', ', $val);
      break;
    }
    case 'userreference_select':
    case 'userreference_buttons':
    case 'nodereference_select':
    case 'nodereference_buttons':
    case 'optionwidgets_select':
    case 'optionwidgets_buttons': {
      if (($type==='userreference_select') || ($type==='userreference_buttons')) $tp='uid';
      elseif (($type==='nodereference_select') || ($type==='nodereference_buttons')) $tp='nid';
      else $tp='value';
      $child=element_children($element[$children[$pos]]['#default_value']);
      $val=array();
      foreach ($child as $elem) {
        if ($element[$children[$pos]]['#default_value'][$elem][$tp]!='') $val[]=$element[$children[$pos]]['#default_value'][$elem][$tp];
      }
      $value=implode(', ', $val);
      break;
    }
    case 'optionwidgets_onoff': {
      if ($element[$children[$pos]]['#default_value'][0]['value']!='') $value=$element[$children[$pos]]['#default_value'][0]['value'];
      break;
    }
    default: {
      if ($element[$children[$pos]]['#multiple']) {
        $value=implode(', ', $element[$children[$pos]]['#default_value']);
      }
      else {
        $value=$element[$children[$pos]]['#default_value'];
      }
    }
  }
  $markup["label"]=$label;
  $markup["value"]=$value;
  return $markup;
}
function elementdefaults_to_disabled(&$element, $children, $pos, $type) {
  $element[$children[$pos] ."_disabled"]=$element[$children[$pos]];
  $element[$children[$pos] ."_disabled"]['#value'] = $element[$children[$pos]]['#default_value'];
  $element[$children[$pos] ."_disabled"]['#attributes']['disabled']='disabled';
  $disabled=drupal_render($element[$children[$pos] ."_disabled"]);
  return $disabled;
}

function elementdefaults_enable_path() {
  $pages = variable_get('elementdefaults_visibility_pages', '');
  $mode = variable_get('elementdefaults_visibility_mode', 0);
  if ($pages) {
    if ($mode < 2) {
      $path = drupal_get_path_alias($_GET['q']);
      $regexp = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1'. preg_quote(variable_get('site_frontpage', 'node'), '/') .'\2'), preg_quote($pages, '/')) .')$/';
      // Compare with the internal and path alias (if any).
      $page_match = preg_match($regexp, $path);
      if ($path != $_GET['q']) {
        $page_match = $page_match || preg_match($regexp, $_GET['q']);
      }
      // When $mode has a value of 0, the transliteration is enabled on
      // all pages except those listed in $pages. When set to 1, it
      // is enabled only on those pages listed in $pages.
      $page_match = !($mode xor $page_match);
    }
    else {
      $page_match = drupal_eval($pages);
    }
  }
  else {
    $page_match = TRUE;
  }
  return $page_match;
}
